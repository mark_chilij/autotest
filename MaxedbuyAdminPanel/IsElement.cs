﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;

namespace MaxedbuyAdminPanel
{
    class IsElement
    {
        public static bool Exists(IWebDriver driver, By iClassName)
        {
            try
            {
                driver.FindElement(iClassName);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
