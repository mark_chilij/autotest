using MaxedbuyAdminPanel;
using Microsoft.VisualBasic.CompilerServices;
using NuGet.Frameworks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MainTests
{
    public class MainTests
    {
        private IWebDriver driver;
        private readonly By _UserName = By.Id("UserName");
        private readonly By _Password = By.Id("Password");
        private readonly By _AdminLoginButton = By.XPath("//input[@class='btn btn-primary']");
        private readonly By _GoToProduct = By.XPath("//a[@href='/Product']");
        private readonly By _NextPage = By.XPath("//li[@class='PagedList-skipToNext']/a");
        private readonly By _ProductCode = By.XPath("//div[4]/div[1]/a");
        private readonly By _InstockClass = By.XPath("//span[@class='a-size-medium a-color-success']");

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://maxedbuy.co.uk:8433/Account/Login");
            WaitUntil.ShouldLocate(driver, "https://maxedbuy.co.uk:8433/Account/Login");

        }


        [Test]
        public void AdminPanel()
        {
            WaitUntil.WaitElement(driver, _UserName);

            var userName = driver.FindElement(_UserName);
            userName.SendKeys("admin");
            var Password = driver.FindElement(_Password);
            Password.SendKeys("m@xed20buy!!!");
            
            WaitUntil.WaitElement(driver, _AdminLoginButton);
            var adminLoginButton = driver.FindElement(_AdminLoginButton);
            adminLoginButton.Click();

        //    var goToProduct = driver.FindElement(_GoToProduct);
      //      goToProduct.Click();
            driver.Navigate().GoToUrl("https://maxedbuy.co.uk:8433/Product?pageNumber=2715");
                
            bool isInStock;
            int count = driver.FindElements(By.TagName("tr")).Count;
            // WaitUntil.WaitSomeInterval();

            for (int i = 1; i < count; i++)
             {
                string element = "//*[@id='partial']/div/table/tbody/tr[" + i + "]/td[4]";
                string element_link = "//*[@id='partial']/div/table/tbody/tr[" + i + "]/td[1]/a";

                string status = driver.FindElement(By.XPath(element)).Text;
                if (status != "True")
                 {
                    string clickelement = driver.FindElement(By.XPath(element_link)).GetAttribute("href");
                    driver.Navigate().GoToUrl(clickelement);
                   WaitUntil.ShouldLocate(driver, clickelement);

                    string productCode = driver.FindElement(_ProductCode).GetAttribute("href");
                    driver.Navigate().GoToUrl(productCode);
                    WaitUntil.ShouldLocate(driver, productCode);
                    isInStock = IsElement.Exists(driver, _InstockClass);    
                  //  Console.WriteLine(isInStock);

                    if(isInStock == true)
                    {
                        //Console.WriteLine("This product in stock:");
                        Console.WriteLine(clickelement);// this product in stock
                    }
                    driver.Navigate().Back();
                    driver.Navigate().Back();
                }
                if (i == count-1)
                {
                    var nextPage = driver.FindElement(_NextPage);
                    nextPage.Click();
                    i = 0;
                    if (!(IsElement.Exists(driver, _NextPage)))
                        break;
                }

            }

        }


        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}